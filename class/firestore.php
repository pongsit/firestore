<?php

namespace pongsit\firestore;

class firestore {
	public function __construct(){

	}

	public function get_token($firestore_key,$email,$password){
		$url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword";
		$postfields  = array(
		    "email" => $email,
		    "password" => $password,
		    "returnSecureToken" => true
		);

		$datapost = json_encode($postfields);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_CUSTOMREQUEST => 'POST',
		    CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
		    CURLOPT_URL => $url . '?key='.$firestore_key,
		    CURLOPT_USERAGENT => 'cURL',
		    CURLOPT_POSTFIELDS => $datapost
		));


		$response = curl_exec( $curl );

		curl_close( $curl );

		$responsJson=json_decode($response,true);

		return $responsJson['idToken'];
	}

	public function update($firestore_key,$email,$password,$project_id,$path,$field,$value,$type="stringValue"){
		// $type: stringValue, doubleValue, integerValue, booleanValue, arrayValue, bytesValue, geoPointValue, mapValue, nullValue, referenceValue, timestampValue

		$token = $this->get_token($firestore_key,$email,$password);

		$data['fields'][$field][$type] = $value;
		$json = json_encode($data);

		$url = "https://firestore.googleapis.com/v1/projects/".$project_id."/databases/(default)/documents/".$path."?key=".$firestore_key."&updateMask.fieldPaths=".$field;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_CUSTOMREQUEST => 'POST',
		    CURLOPT_HTTPHEADER => array('Content-Type: application/json',
		        'Content-Length: ' . strlen($json),
		        'X-HTTP-Method-Override: PATCH',
		        'Authorization: Bearer '.$token      
		    ),
		    CURLOPT_URL => $url,
		    CURLOPT_USERAGENT => 'cURL',
		    CURLOPT_POSTFIELDS => $json
		));


		$response = curl_exec( $curl );

		curl_close( $curl );

		return json_decode($response,true);
	}
	
}

// Firestore rule:
// service cloud.firestore {
//   match /databases/{database}/documents {
//     match /{document=**} {
//       allow read, write: if request.auth.uid == <UID>;
//     }
//   }
// }